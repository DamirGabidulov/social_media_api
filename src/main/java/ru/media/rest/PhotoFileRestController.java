package ru.media.rest;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.S3Object;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.media.dto.PhotoFileDto;
import ru.media.model.PhotoFile;
import ru.media.service.PhotoFileService;
import ru.media.service.UserService;
import ru.media.service.impl.S3Service;
import ru.media.util.FileUtil;

import java.io.ByteArrayOutputStream;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/files")
@Tag(name = "PhotoFileRestController", description = "Контроллер взаимодействия с фотографией пользователя")
public class PhotoFileRestController {

    private final PhotoFileService photoFileService;
    private final S3Service s3Service;
    private final UserService userService;

    @Operation(summary = "Получить список всех фото, доступно только админу")
    @GetMapping
    public ResponseEntity<List<PhotoFileDto>> findAll() {
        List<PhotoFileDto> files = photoFileService.findAll();
        if (files.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.status(200).body(files);
    }

    @Operation(summary = "Получить список всех фото по id пользователя")
    @GetMapping("/users/{userId}")
    public ResponseEntity<?> findAll(@Parameter(description = "id юзера") @PathVariable("userId") Integer userId) {
        if (userService.isUserExist(userId)) {
            List<PhotoFile> files = userService.getById(userId).getPhotos();
            if (files.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return ResponseEntity.status(200).body(PhotoFileDto.from(files));
        } else {
            return ResponseEntity.status(404).body("User with this id doesn't exist");
        }
    }

    @Operation(summary = "Получить список определенное фото по id")
    @GetMapping(value = "/{fileId}")
    public ResponseEntity<?> getFile(@Parameter(description = "id файла") @PathVariable("fileId") Integer fileId,
                                     @Parameter(description = "id юзера") @RequestHeader("userId") Integer userId) {
        if (userService.isFileOwnedByUser(userId, fileId)) {
            PhotoFileDto photoFileDto = photoFileService.getById(fileId);
            try {
                S3Object s3object = s3Service.getS3Object(photoFileDto.getLocation());
                String contentType = s3object.getObjectMetadata().getContentType();
                //переводим inputStream от чтения нашего файла в хранилище амазона в output для отображения в браузере
                ByteArrayOutputStream outputStream = FileUtil.inputStreamToOutputStream(s3object);

                return ResponseEntity.ok()
                        .contentLength(s3object.getObjectMetadata().getContentLength())
                        .contentType(contentType.isEmpty() ? MediaType.APPLICATION_OCTET_STREAM : MediaType.valueOf(contentType))
                        //inline - отображение в браузере, attachment - скачивание
                        .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + photoFileDto.getName() + "\"")
                        .body(outputStream.toByteArray());
            } catch (AmazonS3Exception e) {
                return ResponseEntity.status(404).body("Can't find file in storage. Server message " + e);
            }
        } else {
            return ResponseEntity.status(404).body("User with this id doesn't exist or file doesn't belong to user");
        }
    }

    @Operation(summary = "Сохранить фото")
    @PostMapping
    public ResponseEntity<?> save(@Parameter(description = "файл, который загружаем") @RequestParam("file") MultipartFile file,
                                  @Parameter(description = "id юзера") @RequestHeader("userId") Integer userId) {
        if (userService.isUserExist(userId) & !userService.isFileWithThisNameAlreadyExist(file, userId)) {
            return ResponseEntity.status(202).body(photoFileService.save(file, userId));
        } else {
            return ResponseEntity.status(404).body("User with this id doesn't exist or file doesn't belong to user");
        }
    }

    @Operation(summary = "Обновить фото")
    @PutMapping(value = "/{fileId}")
    public ResponseEntity<?> update(@Parameter(description = "id файла") @PathVariable("fileId") Integer fileDtoId,
                                    @Parameter(description = "файл, который обновляем") @RequestParam("file") MultipartFile file,
                                    @Parameter(description = "id юзера") @RequestHeader("userId") Integer userId) {
        if (userService.isFileOwnedByUser(userId, fileDtoId)) {
            return ResponseEntity.ok().body(photoFileService.update(fileDtoId, file, userId));
        } else {
            return ResponseEntity.status(404).body("User with this id doesn't exist or file doesn't belong to user");
        }
    }

    @Operation(summary = "Удалить фото")
    @DeleteMapping(value = "/{fileId}")
    public ResponseEntity<?> delete(@Parameter(description = "id файла") @PathVariable("fileId") Integer fileDtoId,
                                    @Parameter(description = "id юзера") @RequestHeader("userId") Integer userId) {
        if (userService.isFileOwnedByUser(userId, fileDtoId)) {
            photoFileService.delete(fileDtoId, userId);
            return new ResponseEntity<>("Deleted successful", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("User with this id doesn't exist or file doesn't belong to user", HttpStatus.NOT_FOUND);
        }
    }
}
