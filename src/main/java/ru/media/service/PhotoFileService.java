package ru.media.service;

import org.springframework.web.multipart.MultipartFile;
import ru.media.dto.PhotoFileDto;

import java.util.List;

public interface PhotoFileService {
    List<PhotoFileDto> findAll();

    PhotoFileDto save(MultipartFile file, Integer userId);

    PhotoFileDto getById(Integer fileId);

    PhotoFileDto update(Integer fileDtoId, MultipartFile file, Integer userId);

    void delete(Integer fileDtoId, Integer userId);




}
