package ru.media.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.media.dto.SignUpForm;
import ru.media.dto.UserDto;
import ru.media.dto.UserResponse;
import ru.media.model.Role;
import ru.media.model.Status;
import ru.media.model.User;
import ru.media.repository.UserRepository;
import ru.media.service.UserService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserResponse save(SignUpForm form) {
        if (userRepository.findByUsername(form.getUsername()).isPresent()){
            throw new IllegalArgumentException("Пользователь с таким username уже зарегистрирован!");
        }

        User user = User.builder()
                .username(form.getUsername())
                .email(form.getEmail())
                .password(passwordEncoder.encode(form.getPassword()))
                .status(Status.ACTIVE)
                .build();

        //Добавляем роль для пользователя
        if (form.getRole() == null){
            form.setRole(Role.USER);
        }
        user.setRole(form.getRole());
        userRepository.save(user);

        return UserResponse.builder()
                .id(user.getId())
                .username(user.getUsername())
                .role(user.getRole())
                .build();
    }

    @Override
    public List<UserDto> findAll() {
        return UserDto.from(userRepository.findAllByStatusEquals(Status.ACTIVE));
    }

    @Override
    public UserDto getById(Integer userId) {
        return UserDto.from(userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist")));
    }

    @Override
    public UserDto update(Integer userId, UserDto userDto) {
        if (isUserExist(userId)) {
            User user = User.builder()
                    .id(userDto.getId())
                    .email(userDto.getEmail())
                    .status(userDto.getStatus())
                    .build();
            return UserDto.from(userRepository.save(user));
        } else {
            throw new IllegalArgumentException("User with this id doesn't exist");
        }
    }

    @Override
    public void delete(Integer userId) {
        User user = userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
        user.setStatus(Status.DELETED);
        userRepository.save(user);
    }

    @Override
    public boolean isUserExist(Integer userId) {
        return userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId).isPresent();
    }

    public boolean isFileOwnedByUser(Integer userId, Integer fileId) {
        try {
            User user = userRepository
                    .findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                    .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
            return user.getPhotos().stream()
                    .filter(x -> x.getStatus().equals(Status.ACTIVE))
                    .anyMatch(y -> y.getId().equals(fileId));
        } catch (IllegalArgumentException e){
            //в контроллере уже Response обрабатывает в зависимости от false/true в данном методе
            return false;
        }
    }

    public boolean isFileWithThisNameAlreadyExist(MultipartFile file, Integer userId) {
        User user = userRepository
                .findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
        return user.getPhotos().stream().anyMatch(photo -> photo.getName().equals(file.getOriginalFilename()));
    }
}
