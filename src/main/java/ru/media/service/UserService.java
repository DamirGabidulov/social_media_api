package ru.media.service;

import org.springframework.web.multipart.MultipartFile;
import ru.media.dto.SignUpForm;
import ru.media.dto.UserDto;
import ru.media.dto.UserResponse;

import java.util.List;

public interface UserService {
    UserResponse save(SignUpForm form);

    List<UserDto> findAll();

    UserDto getById(Integer userId);

    boolean isUserExist(Integer userId);

    UserDto update(Integer userId, UserDto userDto);

    void delete(Integer userId);
    boolean isFileOwnedByUser(Integer userId, Integer fileId);

    boolean isFileWithThisNameAlreadyExist(MultipartFile file, Integer userId);
}
