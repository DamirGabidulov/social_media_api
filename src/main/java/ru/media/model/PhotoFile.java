package ru.media.model;

import javax.persistence.*;
import lombok.*;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "photo_files")
public class PhotoFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String location;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    private User user;

    private Status status;
}
