package ru.media.model;

public enum Status {
    ACTIVE, DELETED
}
