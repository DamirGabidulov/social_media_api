package ru.media.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.media.model.PhotoFile;
import ru.media.model.Status;

import java.util.List;
import java.util.stream.Collectors;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PhotoFileDto {
    private Integer id;
    private String location;
    private String name;
    private Status status;

    public static PhotoFileDto from(PhotoFile file){
        return PhotoFileDto.builder()
                .id(file.getId())
                .location(file.getLocation())
                .name(file.getName())
                .status(file.getStatus())
                .build();
    }

    public static List<PhotoFileDto> from(List<PhotoFile> files){
        return files.stream().map(PhotoFileDto::from).collect(Collectors.toList());
    }
}
