package ru.media.dto;

import lombok.Builder;
import lombok.Data;
import ru.media.model.Role;

@Data
@Builder
public class UserResponse {
    private Integer id;
    private String username;
    private Role role;
}
