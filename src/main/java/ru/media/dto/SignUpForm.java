package ru.media.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.media.model.Role;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SignUpForm {
    @NotBlank
    private String username;
    @NotBlank
    private String email;
    @NotBlank
    private String password;
    private Role role;
}
