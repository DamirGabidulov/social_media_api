package ru.media.security.filter;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.media.model.User;
import ru.media.repository.UserRepository;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RequiredArgsConstructor
public class TokenLogoutFilter extends OncePerRequestFilter {

    private final UserRepository userRepository;
    private final static RequestMatcher logoutRequest = new AntPathRequestMatcher("/api/v1/logout", "GET");

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (logoutRequest.matches(request)) {
            String tokenHeader = request.getHeader("Authorization");
            if (tokenHeader != null && tokenHeader.startsWith("Bearer ")) {
                String token = tokenHeader.substring("Bearer ".length());
                User user = userRepository.findUserByToken(token).orElseThrow(() -> new IllegalArgumentException("User is already logged out"));
                user.setToken(null);
                userRepository.save(user);
                SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
            }
        }
        filterChain.doFilter(request, response);
    }
}
