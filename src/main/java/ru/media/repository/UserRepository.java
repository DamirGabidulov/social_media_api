package ru.media.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.media.model.Status;
import ru.media.model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findUserByStatusEqualsAndId(Status status, Integer userId);
    List<User> findAllByStatusEquals(Status status);
    Optional<User> findByUsername(String username);
    Optional<User> findUserByToken(String username);
}
