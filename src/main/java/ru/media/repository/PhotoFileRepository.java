package ru.media.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.media.model.PhotoFile;

public interface PhotoFileRepository extends JpaRepository<PhotoFile, Integer> {
}
